import * as pulumi from '@pulumi/pulumi'

export async function toPromise<T>(out: pulumi.Output<T>): Promise<T> {
  return new Promise(resolve => {
    out.apply(resolve)
  })
}
