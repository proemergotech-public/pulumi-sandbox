// copied from the output of "kubectl api-resources --namespaced=false"
const clusterWideResources = [
  'ComponentStatus',
  'Namespace',
  'Node',
  'PersistentVolume',
  'MutatingWebhookConfiguration',
  'ValidatingWebhookConfiguration',
  'CustomResourceDefinition',
  'APIService',
  'TokenReview',
  'SelfSubjectAccessReview',
  'SelfSubjectRulesReview',
  'SubjectAccessReview',
  'CertificateSigningRequest',
  'ENIConfig',
  'PodSecurityPolicy',
  'NodeMetrics',
  'PodSecurityPolicy',
  'ClusterRoleBinding',
  'ClusterRole',
  'PriorityClass',
  'StorageClass',
  'VolumeAttachment'
]

export function transformNamespace(namespace: string): (o: any) => void {
  return (o: any) => {
    if (clusterWideResources.includes(o.kind)) {
      return
    }

    if (!o.metadata || !o.metadata.namespace) {
      Object.assign(o.metadata || {}, {
        namespace: namespace // TODO: helm template namespace issue - https://github.com/helm/helm/issues/3553
      })
    }
  }
}
