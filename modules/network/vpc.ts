import * as aws from '@pulumi/aws'
import * as pulumi from '@pulumi/pulumi'
import { RunError } from '@pulumi/pulumi/errors'
// @ts-ignore
import * as cidr from 'node-cidr'
import { accessSync } from 'fs'

export interface VpcArgs {
  readonly availabilityZones: string[]
  readonly cidrIP: string
  readonly resourceTags?: aws.Tags
}

export interface Subnet {
  readonly id: pulumi.Output<string>
  readonly az: string
}

export class Vpc extends pulumi.ComponentResource {
  readonly id: pulumi.Output<string>
  readonly cidrBlock: pulumi.Output<string>
  readonly publicRouteTableId: pulumi.Output<string>
  readonly privateRouteTableIds: pulumi.Output<string>[]
  readonly efsID: pulumi.Output<string>
  readonly vpcEndpointID: pulumi.Output<string>

  readonly privateSubnets: Subnet[]
  readonly publicSubnets: Subnet[]
  readonly fixedIpSubnets: Subnet[]

  constructor(name: string, args: VpcArgs, opts?: pulumi.ResourceOptions) {
    super('network:Vpc', name, {}, opts)

    if (args.availabilityZones.length < 1 || args.availabilityZones.length > 4) {
      throw new RunError(`Unsupported number of availability zones for network: ${args.availabilityZones.length}`)
    }

    const cidrBlock = `${args.cidrIP}/16`
    const allSubnets = cidr.cidr.subnets(cidrBlock, 24)
    const privateSubnets = allSubnets.slice(0, args.availabilityZones.length)
    const publicSubnets = allSubnets.slice(100, 100 + args.availabilityZones.length)
    const fixedIpSubnets = allSubnets.slice(200, 200 + args.availabilityZones.length)
    this.cidrBlock = pulumi.output(cidrBlock)

    const vpcName = `${name}-vpc`
    const vpc = new aws.ec2.Vpc(
      vpcName,
      {
        cidrBlock: cidrBlock,
        enableDnsHostnames: true,
        enableDnsSupport: true,
        tags: Object.assign(
          {
            Name: vpcName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    this.id = vpc.id
    this.privateRouteTableIds = []

    const internetGatewayName = `${name}-igw`
    const internetGateway = new aws.ec2.InternetGateway(
      internetGatewayName,
      {
        vpcId: vpc.id,
        tags: Object.assign(
          {
            Name: internetGatewayName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    const efsName = `${name}-efs`
    const efsFileSystem = new aws.efs.FileSystem(
      efsName,
      {
        tags: Object.assign(
          {
            Name: efsName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )
    this.efsID = efsFileSystem.id

    const efsSecurityGroupName = `${name}-securityGroup-efs`
    const efsSecurityGroup = new aws.ec2.SecurityGroup(
      efsSecurityGroupName,
      {
        vpcId: vpc.id,
        ingress: [
          {
            protocol: 'TCP',
            fromPort: 2049,
            toPort: 2049,
            cidrBlocks: [cidrBlock]
          }
        ],
        tags: Object.assign(
          {
            Name: efsSecurityGroupName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    const publicRouteTableName = `${name}-routeTable-public`
    const publicRouteTable = new aws.ec2.RouteTable(
      publicRouteTableName,
      {
        vpcId: vpc.id,
        routes: [
          {
            cidrBlock: '0.0.0.0/0',
            gatewayId: internetGateway.id
          }
        ],
        tags: Object.assign(
          {
            Name: publicRouteTableName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )
    this.publicRouteTableId = publicRouteTable.id

    this.privateSubnets = []
    this.publicSubnets = []
    this.fixedIpSubnets = []
    for (let i = 0; i < args.availabilityZones.length; i++) {
      const az = args.availabilityZones[i]

      // create public subnets
      const publicSubnetName = `${name}-subnet-public-${i}`
      const publicSubnet = new aws.ec2.Subnet(
        publicSubnetName,
        {
          vpcId: vpc.id,
          availabilityZone: az,
          cidrBlock: publicSubnets[i],
          mapPublicIpOnLaunch: false,
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-subnet-public-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )
      this.publicSubnets.push({
        id: publicSubnet.id,
        az: az
      })

      const publicRouteTableAssociation = new aws.ec2.RouteTableAssociation(
        `${name}-rta-public-${i}`,
        {
          subnetId: publicSubnet.id,
          routeTableId: publicRouteTable.id
        },
        { parent: this }
      )

      // create private subnets
      const privateSubnetName = `${name}-subnet-private-${i}`
      const privateSubnet = new aws.ec2.Subnet(
        privateSubnetName,
        {
          vpcId: vpc.id,
          availabilityZone: az,
          cidrBlock: privateSubnets[i],
          mapPublicIpOnLaunch: false,
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-subnet-private-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )
      this.privateSubnets.push({
        id: privateSubnet.id,
        az: az
      })

      // create fixed ip subnets
      const fixedIpSubnetName = `${name}-subnet-fixedIp-${i}`
      const fixedIpSubnet = new aws.ec2.Subnet(
        fixedIpSubnetName,
        {
          vpcId: vpc.id,
          availabilityZone: az,
          cidrBlock: fixedIpSubnets[i],
          mapPublicIpOnLaunch: false,
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-subnet-fixedIp-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )
      this.fixedIpSubnets.push({
        id: fixedIpSubnet.id,
        az: az
      })

      const eipName = `${name}-eip-${i}`
      const eip = new aws.ec2.Eip(
        eipName,
        {
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-eip-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )

      const natName = `${name}-nat-${i}`
      const natGateway = new aws.ec2.NatGateway(
        natName,
        {
          subnetId: publicSubnet.id,
          allocationId: eip.id,
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-nat-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )

      const natRouteTableName = `${name}-routeTable-nat-${i}`
      const natRouteTable = new aws.ec2.RouteTable(
        natRouteTableName,
        {
          vpcId: vpc.id,
          routes: [
            {
              cidrBlock: '0.0.0.0/0',
              natGatewayId: natGateway.id
            }
          ],
          tags: pulumi.output(az).apply(azName => {
            return Object.assign(
              {
                Name: `${name}-routeTable-nat-${azName}`
              },
              args.resourceTags
            )
          })
        },
        { parent: this }
      )
      this.privateRouteTableIds.push(natRouteTable.id)

      const privateRouteTableAssociation = new aws.ec2.RouteTableAssociation(
        `${name}-rta-nat-private-${i}`,
        {
          subnetId: privateSubnet.id,
          routeTableId: natRouteTable.id
        },
        { parent: this }
      )

      const fixedIpRouteTableAssociation = new aws.ec2.RouteTableAssociation(
        `${name}-rta-nat-fixedIp-${i}`,
        {
          subnetId: fixedIpSubnet.id,
          routeTableId: natRouteTable.id
        },
        { parent: this }
      )

      const privateSubnetId = pulumi.all([privateSubnet.id, privateRouteTableAssociation.id]).apply(([id]) => id)

      const mountTarget = new aws.efs.MountTarget(
        `${name}-mountTarget-efs-${i}`,
        {
          fileSystemId: efsFileSystem.id,
          subnetId: privateSubnetId,
          securityGroups: [efsSecurityGroup.id]
        },
        { parent: this }
      )
    }

    const vpcEndpoint = new aws.ec2.VpcEndpoint(
      `${name}-s3-vpcEndpoint`,
      {
        vpcEndpointType: 'Gateway',
        autoAccept: true,
        routeTableIds: this.privateRouteTableIds,
        serviceName: `com.amazonaws.${aws.config.requireRegion()}.s3`,
        vpcId: vpc.id
      },
      { parent: this }
    )
    this.vpcEndpointID = vpcEndpoint.id
  }
}
