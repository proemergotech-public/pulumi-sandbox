import * as aws from '@pulumi/aws'
import * as pulumi from '@pulumi/pulumi'
import * as cloudflare from '../cloudflare'

export interface ClassicLoadBalancerOptions {
  readonly vpcId: pulumi.Input<string>
  readonly subnetIds: pulumi.Input<string>[]
  readonly internal?: boolean

  readonly healthCheck?: {
    healthyThreshold: pulumi.Input<number>
    interval: pulumi.Input<number>
    target: pulumi.Input<string>
    timeout: pulumi.Input<number>
    unhealthyThreshold: pulumi.Input<number>
  }

  readonly listeners: {
    sourcePort: pulumi.Input<number>
    sourceCidr?: pulumi.Input<string>
    targetPort: pulumi.Input<number>
  }[]

  readonly targetSecurityGroupId: pulumi.Input<string>

  readonly cloudflareCNAMEs?: {
    [key: string]: {
      domain: pulumi.Input<string>
      name: pulumi.Input<string>
      proxied?: pulumi.Input<boolean>
    }
  }
  readonly r53CNAMEs?: {
    [key: string]: {
      name: pulumi.Input<string>
      zoneId: pulumi.Input<string>
      ttl: pulumi.Input<number>
    }
  }

  readonly resourceTags?: aws.Tags
}

export class ClassicLoadBalancer extends pulumi.ComponentResource {
  readonly name: pulumi.Output<string>

  constructor(name: string, args: ClassicLoadBalancerOptions, opts?: pulumi.ComponentResourceOptions) {
    super('network:ClassicLoadbalancer', name, args, opts)

    const securityGroup = new aws.ec2.SecurityGroup(
      `${name}-securityGroup`,
      {
        vpcId: args.vpcId,
        ingress: args.listeners.map(l => ({
          fromPort: l.sourcePort,
          toPort: l.sourcePort,
          protocol: 'tcp',
          cidrBlocks: [l.sourceCidr || '0.0.0.0/0'],
          description: `Allow ${l.sourcePort}`
        })),
        egress: [
          {
            fromPort: 0,
            toPort: 0,
            protocol: '-1',
            securityGroups: [args.targetSecurityGroupId],
            description: `Allow all`
          }
        ],
        tags: Object.assign(
          {
            Name: `${name}-securityGroup`
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    const loadBalancer = new aws.elasticloadbalancing.LoadBalancer(
      `${name}`,
      {
        internal: args.internal,
        healthCheck: args.healthCheck,
        securityGroups: [securityGroup.id],
        crossZoneLoadBalancing: true,
        subnets: args.subnetIds,
        listeners: args.listeners.map(l => ({
          instancePort: l.targetPort,
          instanceProtocol: 'tcp',
          lbPort: l.sourcePort,
          lbProtocol: 'tcp'
        })),
        tags: Object.assign(
          {
            Name: `${name}`
          },
          args.resourceTags
        )
      },
      {
        parent: this
      }
    )

    new aws.ec2.SecurityGroupRule(
      `${name}-rule-ig`,
      {
        securityGroupId: args.targetSecurityGroupId,
        type: 'ingress',
        protocol: '-1',
        fromPort: 0,
        toPort: 0,
        sourceSecurityGroupId: securityGroup.id,
        description: `Allow ${name}`
      },
      { parent: this }
    )

    if (args.cloudflareCNAMEs) {
      for (let key in args.cloudflareCNAMEs) {
        const conf = args.cloudflareCNAMEs[key]
        new cloudflare.DNSRecord(
          `${name}-cname-${key}`,
          {
            domain: conf.domain,
            type: 'CNAME',
            name: conf.name,
            content: loadBalancer.dnsName,
            proxied: conf.proxied
          },
          { parent: this, deleteBeforeReplace: true }
        )
      }
    }

    if (args.r53CNAMEs) {
      for (let key in args.r53CNAMEs) {
        const conf = args.r53CNAMEs[key]
        new aws.route53.Record(
          `${name}-cname-${key}-r53`,
          {
            name: conf.name,
            records: [loadBalancer.dnsName],
            type: 'CNAME',
            zoneId: conf.zoneId,
            ttl: conf.ttl
          },
          { parent: this, deleteBeforeReplace: true }
        )
      }
    }

    this.name = loadBalancer.name
  }
}
