import * as aws from '@pulumi/aws'
import * as pulumi from '@pulumi/pulumi'
import * as cloudflare from '../cloudflare'

export interface NetworkLoadBalancerOptions {
  readonly subnetIds: pulumi.Input<string>[]

  readonly cloudflareCNAMEs?: {
    [key: string]: {
      domain: pulumi.Input<string>
      name: pulumi.Input<string>
      proxied?: pulumi.Input<boolean>
    }
  }
  readonly r53CNAMEs?: {
    [key: string]: {
      name: pulumi.Input<string>
      zoneId: pulumi.Input<string>
      ttl: pulumi.Input<number>
    }
  }

  readonly internal?: pulumi.Input<boolean>
  readonly enableCrossZoneLoadBalancing?: pulumi.Input<boolean>
  readonly idleTimeout?: pulumi.Input<number>

  readonly resourceTags?: aws.Tags
}

export class NetworkLoadBalancer extends pulumi.ComponentResource {
  readonly arn: pulumi.Output<string>

  constructor(name: string, args: NetworkLoadBalancerOptions, opts?: pulumi.ComponentResourceOptions) {
    super('network:Loadbalancer', name, args, opts)

    const loadBalancer = new aws.elasticloadbalancingv2.LoadBalancer(
      `${name}`,
      {
        internal: args.internal,

        enableCrossZoneLoadBalancing: args.enableCrossZoneLoadBalancing,
        idleTimeout: args.idleTimeout,
        loadBalancerType: 'network',
        subnets: args.subnetIds,
        tags: Object.assign(
          {
            Name: `${name}`
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    if (args.cloudflareCNAMEs) {
      for (let key in args.cloudflareCNAMEs) {
        const conf = args.cloudflareCNAMEs[key]
        new cloudflare.DNSRecord(
          `${name}-cname-${key}`,
          {
            domain: conf.domain,
            type: 'CNAME',
            name: conf.name,
            content: loadBalancer.dnsName,
            proxied: conf.proxied
          },
          { parent: this, deleteBeforeReplace: true }
        )
      }
    }

    if (args.r53CNAMEs) {
      for (let key in args.r53CNAMEs) {
        const conf = args.r53CNAMEs[key]
        new aws.route53.Record(
          `${name}-cname-${key}-r53`,
          {
            name: conf.name,
            records: [loadBalancer.dnsName],
            type: 'CNAME',
            zoneId: conf.zoneId,
            ttl: conf.ttl
          },
          { parent: this, deleteBeforeReplace: true }
        )
      }
    }

    this.arn = loadBalancer.arn
  }
}
