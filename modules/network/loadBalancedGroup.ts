import * as aws from '@pulumi/aws'
import * as pulumi from '@pulumi/pulumi'

export interface LoadBalancedGroupOptions {
  readonly vpcId: pulumi.Input<string>
  readonly loadBalancerArn: pulumi.Input<string>

  readonly source: {
    port: pulumi.Input<number>
    cidr?: pulumi.Input<string>
  }

  readonly target: {
    port: number
    securityGroupId: pulumi.Input<string>
  }

  readonly healthCheck?: {
    healthyThreshold?: pulumi.Input<number>
    interval?: pulumi.Input<number>
    matcher?: pulumi.Input<string>
    path?: pulumi.Input<string>
    port?: string
    protocol?: pulumi.Input<string>
    timeout?: pulumi.Input<number>
    unhealthyThreshold?: pulumi.Input<number>
  }

  readonly resourceTags?: aws.Tags
}

export class LoadBalancedGroup extends pulumi.ComponentResource {
  readonly targetGroupArn: pulumi.Output<string>

  constructor(name: string, args: LoadBalancedGroupOptions, opts?: pulumi.ComponentResourceOptions) {
    super('network:LoadBalancedGroup', name, args, opts)

    const targetGroup = new aws.elasticloadbalancingv2.TargetGroup(
      name,
      {
        vpcId: args.vpcId,
        healthCheck: args.healthCheck,
        port: args.target.port,
        protocol: 'TCP',
        tags: Object.assign(
          {
            Name: name
          },
          args.resourceTags
        )
      },
      { parent: this }
    )

    new aws.elasticloadbalancingv2.Listener(
      `${name}-listener`,
      {
        loadBalancerArn: args.loadBalancerArn,
        protocol: 'TCP',
        port: args.source.port,
        defaultAction: {
          type: 'forward',
          targetGroupArn: targetGroup.arn
        }
      },
      { parent: this }
    )

    new aws.ec2.SecurityGroupRule(
      `${name}-securitGroupRule`,
      {
        securityGroupId: args.target.securityGroupId,
        type: 'ingress',
        protocol: 'tcp',
        fromPort: args.target.port,
        toPort: args.target.port,
        cidrBlocks: [args.source.cidr || '0.0.0.0/0'],
        description: `Allow ${name}`
      },
      { parent: this }
    )

    if (args.healthCheck && args.healthCheck.port) {
      let healthcheckPort = -1
      try {
        healthcheckPort = parseInt(args.healthCheck.port)
      } catch (e) {
        // targetGroup.healthcheck.port - (Optional) The port to use to connect with the target.
        // Valid values are either ports 1-65536, or traffic-port. Defaults to traffic-port.
        // So it's either invalid and TargetGroup creation will fail, or 'traffic-port', in which
        // case there is no need for opening another port on the security group.
      }
      if (healthcheckPort > 0 && healthcheckPort != args.target.port) {
        new aws.ec2.SecurityGroupRule(
          `${name}-securitGroupRule-healthcheck`,
          {
            securityGroupId: args.target.securityGroupId,
            type: 'ingress',
            protocol: 'tcp',
            fromPort: healthcheckPort,
            toPort: healthcheckPort,
            cidrBlocks: [args.source.cidr || '0.0.0.0/0'],
            description: `Allow healthcheck for ${name}`
          },
          { parent: this }
        )
      }
    }

    this.targetGroupArn = targetGroup.arn
  }
}
