// @ts-ignore
import * as cloudflare from 'cloudflare'
import * as pulumi from '@pulumi/pulumi'
import * as dynamic from '@pulumi/pulumi/dynamic'
import { RunError } from '@pulumi/pulumi/errors'

class DNSProvider implements dynamic.ResourceProvider {
  create = async (inputs: any) => {
    let client = getClient(inputs.domain)
    let domains = await getDomains(client)

    // need to remove undefined properties:
    // https://github.com/pulumi/pulumi/issues/2205
    const record: any = Object.keys(inputs)
      .filter(k => k !== '__provider' && inputs[k] !== undefined)
      .reduce((acc, k) => ({ ...acc, [k]: inputs[k] }), {})

    const zoneID = domains[record.domain]

    let res
    try {
      res = await client.dnsRecords.add(zoneID, record)
    } catch (e) {
      throw new RunError(`Unable to add DNS record: ${e}`)
    }

    return {
      id: `dns:${inputs.domain}:${zoneID}:${res.result.id}`,
      outs: record
    }
  }

  read = async (id: pulumi.ID, props?: any): Promise<dynamic.ReadResult> => {
    const data = await parseID(id)

    let client = getClient(data.domain)

    try {
      let record = await client.dnsRecords.read(data.zoneID, data.recordID)
      let res = record.result

      const resProps: any = {
        domain: res.zone_name,
        type: res.type,
        name: res.name.replace(`.${res.zone_name}`, ''),
        content: res.content,
        ttl: res.ttl,
        priority: res.priority,
        proxied: res.proxied
      }

      // need to remove undefined properties:
      // https://github.com/pulumi/pulumi/issues/2205
      const props: any = Object.keys(resProps)
        .filter(k => resProps[k] !== undefined)
        .reduce((acc, k) => ({ ...acc, [k]: resProps[k] }), {})

      return {
        id: id,
        props: props
      }
    } catch (e) {
      const exists = await exist(client, data.zoneID, data.recordID)
      if (!exists) {
        return {}
      }

      throw new RunError(`Unable to read DNS record: ${e}`)
    }
  }

  update = async (id: pulumi.ID, olds: any, news: any) => {
    const data = await parseID(id)

    const record = {
      type: news.type,
      name: news.name,
      content: news.content,
      ttl: news.ttl,
      priority: news.priority,
      proxied: news.proxied
    }

    try {
      await getClient(data.domain).dnsRecords.edit(data.zoneID, data.recordID, record)
    } catch (e) {
      throw new RunError(`Unable to update DNS record: ${e}`)
    }

    return {}
  }

  delete = async (id: pulumi.ID, props: any) => {
    const data = await parseID(id)

    try {
      await getClient(data.domain).dnsRecords.del(data.zoneID, data.recordID)
    } catch (e) {
      throw new RunError(`Unable to delete DNS record: ${e}`)
    }
  }
}

export interface DNSRecordArgs {
  domain: pulumi.Input<string>
  type: pulumi.Input<string>
  name: pulumi.Input<string>
  content: pulumi.Input<string>
  ttl?: pulumi.Input<number>
  priority?: pulumi.Input<number>
  proxied?: pulumi.Input<boolean>
}

export class DNSRecord extends dynamic.Resource {
  constructor(name: string, args: DNSRecordArgs, opts?: pulumi.CustomResourceOptions) {
    super(new DNSProvider(), name, args, opts)
  }
}

function getClient(domain: any) {
  if (domain === 'camplace.com') {
    return new cloudflare({
      email: process.env.PROD_CLOUDFLARE_EMAIL as string,
      key: process.env.PROD_CLOUDFLARE_TOKEN as string
    })
  } else {
    return new cloudflare({
      email: process.env.CLOUDFLARE_EMAIL as string,
      key: process.env.CLOUDFLARE_TOKEN as string
    })
  }
}

async function getDomains(client: cloudflare) {
  let domains: {
    [domain: string]: string
  } = {}

  let zones
  try {
    zones = await client.zones.browse()
  } catch (e) {
    throw new RunError(`Exception happened when retrieving zone information: ${e}`)
  }

  for (let zone of zones.result) {
    domains[zone.name] = zone.id
  }

  return domains
}

function parseID(id: string) {
  const parts = id.split(':')

  if (parts.length !== 4) {
    throw new RunError(`Invalid id: ${id}`)
  }

  return {
    domain: parts[1],
    zoneID: parts[2],
    recordID: parts[3]
  }
}

async function exist(client: cloudflare, zoneID: string, recordID: string) {
  try {
    const dnsRecords = await client.dnsRecords.browse(zoneID)

    for (let r of dnsRecords.result) {
      if (r.id === recordID) {
        return true
      }
    }
  } catch (e) {
    throw new RunError(`Exception happened when retrieving dns information: ${e}`)
  }

  return false
}
