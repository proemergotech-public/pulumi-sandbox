import * as pulumi from '@pulumi/pulumi'
import * as aws from '@pulumi/aws'

export interface WorkerNodeGroupOptions {
  /**
   * The service role used by the EKS cluster.
   */
  readonly clusterInstanceRole: pulumi.Input<aws.iam.Role>

  /**
   * The subnets to attach to the worker nodes cluster.
   */
  readonly subnetIds: pulumi.Input<pulumi.Input<string>[]>

  /**
   * The size in GiB of a cluster node's root volume.
   */
  readonly nodeRootVolumeSize: pulumi.Input<number>

  readonly clusterName: pulumi.Input<string>
  readonly clusterEndPoint: pulumi.Input<string>
  readonly clusterCertificateAuthority: pulumi.Input<{
    data: string
  }>
  readonly clusterVersion: pulumi.Input<string>
  readonly nodeSecurityGroupID: pulumi.Input<string>
  readonly instanceType: pulumi.Input<aws.ec2.InstanceType>
  readonly minSize: pulumi.Input<number>
  readonly maxSize: pulumi.Input<number>
  readonly kubeletExtraArgs: pulumi.Input<string>
  readonly nodePublicKeyName?: pulumi.Input<string>
  readonly resourceTags?: aws.Tags

  readonly targetGroupArns?: pulumi.Input<pulumi.Input<string>[]>
  readonly classicLoadBalancerNames?: pulumi.Input<pulumi.Input<string>[]>
}

export class WorkerNodeGroup extends pulumi.ComponentResource {
  readonly autoScalingGroupName: pulumi.Output<string>

  constructor(name: string, args: WorkerNodeGroupOptions, opts?: pulumi.ComponentResourceOptions) {
    super('eks:index:WorkerNodeGroup', name, args, opts)

    const instanceProfile = new aws.iam.InstanceProfile(
      `${name}-instanceProfile`,
      {
        role: args.clusterInstanceRole
      },
      { parent: this }
    )

    const awsRegion = pulumi.output(aws.getRegion({}, { parent: this }))

    const userdata = pulumi
      .all([awsRegion, args.clusterName, args.clusterEndPoint, args.clusterCertificateAuthority])
      .apply(([region, clusterName, clusterEndpoint, clusterCa]) => {
        return `#!/bin/bash

                /etc/eks/bootstrap.sh --apiserver-endpoint "${clusterEndpoint}" \\
                --b64-cluster-ca "${clusterCa.data}" --kubelet-extra-args "${args.kubeletExtraArgs}" "${clusterName}"
                `
      })

    const eksWorkerAmi = pulumi.all([args.clusterVersion, args.instanceType]).apply(([clusterVersion, instanceType]) => {
      const isGPU = /^p[2-3]\./.test(instanceType) // eks only support p2/p3 instances for GPU ami
      return this.getAmi(clusterVersion, isGPU)
    })

    const nodeLaunchConfiguration = new aws.ec2.LaunchConfiguration(
      `${name}-launchConfiguration`,
      {
        associatePublicIpAddress: true,
        imageId: eksWorkerAmi.apply(r => r.imageId),
        instanceType: args.instanceType,
        iamInstanceProfile: instanceProfile.id,
        keyName: args.nodePublicKeyName,
        securityGroups: [args.nodeSecurityGroupID],
        rootBlockDevice: {
          volumeSize: args.nodeRootVolumeSize,
          volumeType: 'gp2',
          deleteOnTermination: true
        },
        userData: userdata
      },
      { parent: this }
    )

    const autoScalingGroup = new aws.autoscaling.Group(
      `${name}-autoScalingGroup`,
      {
        desiredCapacity: args.minSize,
        launchConfiguration: nodeLaunchConfiguration,
        minSize: args.minSize,
        maxSize: args.maxSize,
        vpcZoneIdentifiers: args.subnetIds,
        tags: [
          {
            key: 'Name',
            value: `${name}-worker`,
            propagateAtLaunch: true
          },
          {
            key: pulumi.output(args.clusterName).apply(c => `kubernetes.io/cluster/${c}`),
            value: `owned`,
            propagateAtLaunch: true
          }
        ],
        targetGroupArns: args.targetGroupArns,
        loadBalancers: args.classicLoadBalancerNames
      },
      { parent: this }
    )

    this.autoScalingGroupName = autoScalingGroup.name
  }

  private getAmi = (clusterVersion: string, isGPU: boolean): Promise<aws.GetAmiResult> => {
    let owner = '602401143452'
    let namePrefix = `amazon-eks-node-${clusterVersion}-*`

    if (isGPU) {
      owner = '679593333241'
      namePrefix = `amazon-eks-gpu-node-${clusterVersion}-*`
    }

    return aws.getAmi(
      {
        filters: [
          {
            name: 'name',
            values: [`${namePrefix}`]
          }
        ],
        mostRecent: true,
        owners: [`${owner}`]
      },
      { parent: this }
    )
  }
}
