import * as aws from '@pulumi/aws'
import * as k8s from '@pulumi/kubernetes'
import * as pulumi from '@pulumi/pulumi'
import * as yaml from 'js-yaml'

import { ServiceRole } from './serviceRole'

export interface ClusterOptions {
  readonly vpcId: pulumi.Input<string>
  readonly subnetIds: pulumi.Input<pulumi.Input<string>[]>
  readonly kubernetesAdminRoleARN: pulumi.Input<string>
  readonly adminRoleARN: pulumi.Input<string>
  readonly resourceTags?: aws.Tags
}

export class Cluster extends pulumi.ComponentResource {
  readonly kubeconfig: pulumi.Output<any>

  readonly provider: k8s.Provider

  readonly securityGroup: aws.ec2.SecurityGroup
  readonly sharedWorkerInstanceRole: pulumi.Output<aws.iam.Role>
  readonly name: pulumi.Output<string>
  readonly endpoint: pulumi.Output<string>
  readonly certificateAuthority: pulumi.Output<{
    data: string
  }>
  readonly version: pulumi.Output<string>

  constructor(name: string, args: ClusterOptions, opts?: pulumi.ComponentResourceOptions) {
    super('eks:Cluster', name, args, opts)

    const sharedAdminProvider = new aws.Provider(
      `${name}-eks-admin-provider`,
      {
        region: aws.config.requireRegion(),
        assumeRole: {
          roleArn: args.adminRoleARN
        }
      },
      {
        parent: this
      }
    )

    const eksRole = new ServiceRole(
      `${name}-serviceRole-eks`,
      {
        service: 'eks.amazonaws.com',
        description: 'Allows EKS to manage clusters on your behalf.',
        managedPolicyArns: ['arn:aws:iam::aws:policy/AmazonEKSClusterPolicy', 'arn:aws:iam::aws:policy/AmazonEKSServicePolicy']
      },
      { parent: this }
    )

    const allEgress = {
      description: 'Allow internet access.',
      fromPort: 0,
      toPort: 0,
      protocol: '-1', // all
      cidrBlocks: ['0.0.0.0/0']
    }

    const eksClusterSecurityGroupName = `${name}-securityGroup-eks`
    const eksClusterSecurityGroup = new aws.ec2.SecurityGroup(
      eksClusterSecurityGroupName,
      {
        vpcId: args.vpcId,
        egress: [allEgress],
        tags: Object.assign(
          {
            Name: eksClusterSecurityGroupName
          },
          args.resourceTags
        )
      },
      { parent: this }
    )
    this.securityGroup = eksClusterSecurityGroup

    const eksCluster = new aws.eks.Cluster(
      `${name}-cluster-eks`,
      {
        roleArn: eksRole.role.apply(r => r.arn),
        vpcConfig: {
          securityGroupIds: [eksClusterSecurityGroup.id],
          subnetIds: args.subnetIds
        },
        version: '1.11'
      },
      {
        parent: this,
        provider: sharedAdminProvider
      }
    )

    this.name = eksCluster.name
    this.endpoint = eksCluster.endpoint
    this.certificateAuthority = eksCluster.certificateAuthority
    this.version = eksCluster.version

    this.sharedWorkerInstanceRole = new ServiceRole(
      `${name}-serviceRole-workers`,
      {
        service: 'ec2.amazonaws.com',
        managedPolicyArns: [
          'arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy',
          'arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy',
          'arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly'
        ]
      },
      { parent: this }
    ).role
    const sharedWorkerInstanceRoleARN = this.sharedWorkerInstanceRole.apply(r => r.arn)

    const myKubeconfig = pulumi
      .all([eksCluster.name, eksCluster.endpoint, eksCluster.certificateAuthority, args.adminRoleARN])
      .apply(([clusterName, clusterEndpoint, clusterCertificateAuthority, sharedAdminRoleArn]) => {
        return this.createKubernetesConfig(clusterName, clusterEndpoint, clusterCertificateAuthority, sharedAdminRoleArn)
      })

    const k8sProvider = new k8s.Provider(
      `${name}-eks-k8s`,
      {
        kubeconfig: myKubeconfig.apply(JSON.stringify)
      },
      { parent: this }
    )

    const eksNodeAccess = new k8s.core.v1.ConfigMap(
      `${name}-configMap-aws-auth`,
      {
        apiVersion: 'v1',
        metadata: {
          name: 'aws-auth',
          namespace: 'kube-system'
        },
        data: {
          mapRoles: pulumi
            .all([sharedWorkerInstanceRoleARN, args.kubernetesAdminRoleARN])
            .apply(([sharedWorkerInstanceRoleARN, kubernetesAdminRoleARN]) =>
              yaml.safeDump(
                [
                  {
                    rolearn: `${sharedWorkerInstanceRoleARN}`,
                    username: 'system:node:{{EC2PrivateDNSName}}',
                    groups: ['system:bootstrappers', 'system:nodes']
                  },
                  {
                    rolearn: `${kubernetesAdminRoleARN}`,
                    username: 'kubernetes-admin',
                    groups: ['system:masters']
                  }
                ],
                {
                  lineWidth: 150
                }
              )
            )
        }
      },
      { parent: this, provider: k8sProvider, dependsOn: eksCluster }
    )

    this.kubeconfig = pulumi
      .all([eksNodeAccess.id, eksCluster.name, myKubeconfig, args.kubernetesAdminRoleARN])
      .apply(([_, clusterName, myKubeconfig, kubernetesAdminRoleARN]) => {
        return this.createFinalKubernetesConfig(myKubeconfig, clusterName, kubernetesAdminRoleARN)
      })

    this.provider = new k8s.Provider(
      `${name}-provider`,
      {
        kubeconfig: this.kubeconfig.apply(JSON.stringify)
      },
      { parent: this }
    )

    this.registerOutputs({ kubeconfig: this.kubeconfig })
  }

  private createKubernetesConfig = (
    clusterName: string,
    clusterEndpoint: string,
    clusterCertificateAuthority: { data: string },
    sharedAdminRoleArn: string
  ) => {
    let exec = {
      apiVersion: 'client.authentication.k8s.io/v1alpha1',
      command: 'aws-iam-authenticator',
      args: ['token', '-i', clusterName, '-r', sharedAdminRoleArn]
    }

    return {
      apiVersion: 'v1',
      clusters: [
        {
          cluster: {
            server: clusterEndpoint,
            'certificate-authority-data': clusterCertificateAuthority.data
          },
          name: clusterName
        }
      ],
      contexts: [
        {
          context: {
            cluster: clusterName,
            user: clusterName
          },
          name: clusterName
        }
      ],
      'current-context': clusterName,
      kind: 'Config',
      users: [
        {
          name: clusterName,
          user: {
            exec: exec
          }
        }
      ]
    }
  }

  private createFinalKubernetesConfig = (original: any, clusterName: string, kubernetesAdminRoleARN: string) => {
    let newConf = JSON.parse(JSON.stringify(original))
    let exec = {
      apiVersion: 'client.authentication.k8s.io/v1alpha1',
      command: 'aws-iam-authenticator',
      args: ['token', '-i', clusterName, '-r', kubernetesAdminRoleARN]
    }

    newConf.users[0].user.exec = exec

    return newConf
  }
}
