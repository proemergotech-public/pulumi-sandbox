import * as eks from '../modules/eks'
import * as network from '../modules/network'
import * as pulumi from '@pulumi/pulumi'
import * as yaml from 'js-yaml'
import * as aws from '@pulumi/aws'
import { config } from './config'
import { Output } from './output'

const stack = pulumi.getStack()

async function run(): Promise<pulumi.Lifted<Output>> {
  const availabilityZones = await aws.getAvailabilityZones().then(result => result.names.slice(0, config.network.numberOfAvailabilityZones))

  const eksAdminRole = new aws.iam.Role(`${stack}-eks-admin-role`, {
    assumeRolePolicy: JSON.stringify({
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Principal: {
            AWS: config.principalUser
          },
          Action: 'sts:AssumeRole',
          Condition: {}
        }
      ]
    })
  })

  const eksAdminPolicy = new aws.iam.Policy(`${stack}-eks-admin-policy`, {
    policy: JSON.stringify({
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Action: ['iam:GetRole', 'iam:PassRole', 'eks:*'],
          Resource: '*'
        }
      ]
    })
  })

  new aws.iam.PolicyAttachment(`${stack}-eks-admin-policy-attachment`, {
    policyArn: eksAdminPolicy.arn,
    roles: [eksAdminRole]
  })

  const kubernetesAdminRole = new aws.iam.Role(`${stack}-kubernetes-admin-role`, {
    assumeRolePolicy: JSON.stringify({
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Principal: {
            AWS: config.principalUser
          },
          Action: 'sts:AssumeRole',
          Condition: {}
        }
      ]
    })
  })

  const vpc = new network.Vpc(`${stack}`, {
    availabilityZones: availabilityZones,
    cidrIP: config.network.cidrIP
  })

  const cluster = new eks.Cluster(`${stack}`, {
    vpcId: vpc.id,
    subnetIds: vpc.privateSubnets.concat(vpc.publicSubnets).map(s => s.id),
    adminRoleARN: eksAdminRole.arn,
    kubernetesAdminRoleARN: eksAdminRole.arn
  })

  const kubeconfig = cluster.kubeconfig.apply(k =>
    yaml.safeDump(k, {
      lineWidth: 150
    })
  )

  return {
    efsId: vpc.efsID,
    kubeconfig: kubeconfig,
    vpc: pulumi.output({
      id: vpc.id,
      cidrBlock: vpc.cidrBlock,
      fixedIpSubnetIds: pulumi.all(vpc.fixedIpSubnets.map(s => s.id)),
      privateIpSubnetIds: pulumi.all(vpc.privateSubnets.map(s => s.id)),
      publicIpSubnetIds: pulumi.all(vpc.publicSubnets.map(s => s.id))
    }),
    cluster: pulumi.output({
      name: cluster.name,
      endpoint: cluster.endpoint,
      certificateAuthority: cluster.certificateAuthority.data
    })
  }
}

export const output = pulumi.output(run())
