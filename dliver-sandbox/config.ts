import * as pulumi from '@pulumi/pulumi'

const principalUser = new pulumi.Config('dliver-sandbox').require('principalUser')

export const config: Config = {
  principalUser: principalUser,
  network: {
    cidrIP: '10.151.0.0',
    numberOfAvailabilityZones: 2
  }
}

interface Config {
  principalUser: string
  network: {
    cidrIP: string
    numberOfAvailabilityZones: number
  }
}
