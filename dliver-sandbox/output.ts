export interface Output {
  efsId?: string
  kubeconfig?: string
  vpc?: {
    id?: string
    cidrBlock?: string
    fixedIpSubnetIds?: string[]
    privateIpSubnetIds?: string[]
    publicIpSubnetIds?: string[]
  }
  cluster?: {
    name?: string
    endpoint?: string
    certificateAuthority?: string
  }
}
